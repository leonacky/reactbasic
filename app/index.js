/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {Button} from 'react-native-elements'

class Form extends Component {
    render() {
        return (
            <View>
                <Button small title='Click Me' onPress={this.props.clickButton}/>
                <Text>
                    You have pressed the button {this.props.counter}
                    times!
                </Text>
            </View>
        );
    }
}

var App = React.createClass({
    getInitialState: function() {
        return {counter: 0}
    },
    clickButton: function() {
        var newCount = this.state.counter += 1;
        this.setState({counter: newCount});
    },
    render: function() {
        return (
            <View style={styles.container}>
                <Text>
                    Welcome to the counter app!
                </Text>
                <Form counter={this.state.counter} clickButton={this.clickButton}/>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});

export default App;
